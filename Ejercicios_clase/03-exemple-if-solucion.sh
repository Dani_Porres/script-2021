#! /bin/bash
# @Dani_Porres
# 03 Febrero 2022
# Ejemplo if : indica si es mayor de edad
#--------------------------------

if [ $# -ne 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

#2) Xixa
edat=$1
if [ $edat -ge 18 ]
then
  echo "edat $edat es mayor de edad"
else
  echo "edat $edat no es mayor de edad"
fi
exit 0
