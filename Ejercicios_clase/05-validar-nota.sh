#! /bin/bash
# @Dani_Porres
# 03 Febrero 2022
#--------------------------------
#1) si num args no es correcto cerrar
ERR_NARGS=1
ERR_NOTA=2

if [ $# -ne 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

#2) Validar rangos nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error nota $1 no valida"
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  echo $ERR_NOTA
fi 
#3) xixa


