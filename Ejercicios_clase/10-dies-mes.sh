#! /bin/bash
# @Dani_Porres
# 03 Febrero 2022
#--------------------------------

ERR_ARGS=1
ERR_NOTA=2
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 nota"
  exit $ERR_ARGS
fi

#2 validar si help
if [ "$1" = "-h" -o "$1" = "--help" ]
then
	echo "Escola del treball"
	echo "ASXI-M01"
	echo "Usage: $0 mes"
	exit $OK
fi

#3) validar arg pren valors
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
	echo "Error, mes $mes no valid"
	echo "Mes pren valors del [1-12]"
	echo "Usage: $0 mes"
	exit $ERR_ARGVL
fi

#4)xixa: determina numero dies del mes

case "$mes" in
	"2")
	  dies=28;;
	"4"|"6"|"9"|"11"

