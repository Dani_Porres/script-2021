#! /bin/bash
# @Dani_Porres
# 03 Febrero 2022
# Descripcion: bucles for
#--------------------------------

#7) Listar logins numerados
llistar_noms=$(cut -d: -f1 /etc/passwd | sort)
num=1
for elem in $llistar_noms
do
	echo "$num: $elem"
	((num++))
done
exit 0


#6)Listar numerado los nombres dels files
# del directorio activo
llistar_noms=$(ls)
num=1
for elem in $llistar_noms
do
	echo "$num: $elem"
	((num++))
done
exit 0

#5) llistar els args numerats
num=0
for arg in $*
do
	num=$(($num+1))
	echo "$num: $arg"
done
exit 0 


#4)iterar por la lista de argumentos 
# El $@ expande las palabras aunque las 
# palabras esten entre comillas
for arg in "$@"
do 
	echo $arg
done
exit 0


#3)iterar pel valor d'una variable
llistar=$(ls)
for nom in $llistar
do
	echo $nom
done
exit 0


#2)intentar per un conjunt d'elements (los junta en la misma linea)
for nom in "pere pau marta anna"
do 
	echo "$nom"
done
exit 0

#1)intentar per un conjunt d'elements (lo printa en lineas diferentes)
for nom in "pere" "pau" "marta" "anna"
do 
	echo "$nom"
done
exit 0
