#! /bin/bash
# @Dani_Porres
# 03 Febrero 2022
# Descripcion: le entra una nota y dice si estas aprobado o suspendido
#--------------------------------

ERR_NOTA=2

if [ $# -ne 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi
#2) Validar rangos nota

for nota in $*
do
	if [ $nota -ge 0 -a $nota -le 10 ]
	then
		echo "Error: nota $nota no valida" >> /dev/stderr
	elif [ $nota -lt 5 ]
	then
		echo "Suspes
	elif [ $nota -lt 7 ]
	then
		echo "Aprovat"
	elif [ $nota -lt 9 ]
	then
		echo "Notable"
	else
		echo "Excelente"
	fi
done
exit 0

