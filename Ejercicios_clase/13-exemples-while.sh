#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Descrip: ejemplos while
#--------------------------------

#7) numerar stdin linea a linea i pasar a mayus

#6) procesar stdin fins al token FI

read -r line
while [ "$line" != "FI" ]
do
  echo "$line"
  read -r line
done
exit 0

#5) numerar stdin linea a linea

num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

#4)Procesar entrada estandar linia a linia

while read -r line
do
  echo $line
done
exit 0

#3) itenar arguments amb shift

while [ -n "$1" ]
do
  echo "$1, $#, $*"
  shift
done

#2) cuenta del 10 al 1
MIN=0
num=$1
while [ $num -ge $MIN ]
do
  echo -n "$num, "
  ((num--))
done
exit 0

#1) cuenta del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done
exit 0
