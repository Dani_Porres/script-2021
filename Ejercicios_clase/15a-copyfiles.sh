#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Descripcion: copiar fileorigen a dirdesti
#-------------------------------------------

# Validar 2 args
ERR_NARGS=1
ERR_NODIR=2
ERR_NOREGULAR=3
OK=0

if [ $# -lt 2 ]; then
  echo "Error: num args incorrecto"
  echo "usage $0 file[...] dir-desti"
  exit $ERR_NARGS
fi
# primer arg es un file
if [ ! -f $file ]; then
  echo "Error: $file no és un regular file"
  echo "usage $0 file[...] dir-destí"
  exit $ERR_NOREGULARFILE
fi


# segon es un dir existent

if [ ! -f $desti ]; then
  echo "Error: $desti no e sun regular file"
  echo "usage $0 file[...] dir-desti"
  exit $ERR_NODIR
fi


cp $file $desti
exit 0

