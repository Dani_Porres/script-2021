#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Descripcion: detectar argumentos
#-------------------------------------------
ERR_NARGS=1

if [ $# -eq 0 ]
then
  echo "Error: numero args incorrecto"
  echo "Usage: $0 [-a -b -c -d -e -f] args[]" 
  exit $ERR_NARGS
fi

opcio=""
arg="" 
for args in $*
do
  case $args in
  "-a"|"-b"|"-c"|"-d"|"-e"|"-f" )
	  opcio="$opcio $arg" ;;
*)
	arg="$args $arg" ;;
esac
