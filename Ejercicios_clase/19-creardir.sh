#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Descripcion: Crear directorio 
#
# almenos un arg
# mkdir no genera sortida
# 0 todo directorio creado ok
# 1 error en el numero de argumentos 
# 2 si algun directorio no se pudo crear 
#-----------------------------------------

ERR_ARGS=1
ERRO_DIR=2
STATUS=0

if [ $# -lt 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 arg [.....]"
  echo "$ERR_ARGS"
  exit $ERR_ARGS
fi


for nom in $*
do
  mkdir -p $nom &> /dev/null
  if [ $? -ne 0 ]; then
     echo "Error: no se creo $nom" >&2
     STATUS=$ERR_DIR
  fi
done
exit $STATUS
