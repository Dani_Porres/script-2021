#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Ejemplo processament arguments
#--------------------------------

ERR_NARGS=1

# Validar arguments

if [ $# -ne 0 ]
then
  echo "ERROR: numero de argumentos incorrecto"
  echo "USAGE: $0 argumentos"
  exit $ERR_NARGS
fi

# Entrada linea a linea
num=1
while read -r line
do      
  echo "$num: $line "
  ((num++))
done
exit 0

