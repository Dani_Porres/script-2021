#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Ejemplo processament arguments
#--------------------------------


if [ $# -lt 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog arg1 arg2"
  exit 1
fi
# xixa

num=1
for arg in $*
do
  echo "$num: $arg"
  num=$(($num+1))
done
exit 0

