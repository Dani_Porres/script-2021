#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Contador de zeros
#--------------------------------

# Validar arguments
ERR_NARGS=1
if [ $# -ne 1 ]
then
  echo "ERROR: numero de argumentos incorrecto"
  echo "USAGE: $0 argumentos"
  exit $ERR_NARGS
fi
# XIXAAA

MAX=$1
num=0
while [ $num -le $MAX ] 
do
  echo "$num"
  ((num++))
done	
exit 0

