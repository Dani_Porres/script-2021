#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Programa arguments de mes
#--------------------------------

# Validar arguments
ERR_NARGS=1
if [ $# -ne 1 ]
then
  echo "ERROR: numero de argumentos incorrecto"
  echo "USAGE: $0 argumentos"
  exit $ERR_NARGS
fi
# Validar argumento en mes del 1 al 12
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes $mes no vàlid"
  echo "Mes pren valors del [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_ARGVL
fi

#Determinamos los meses y sus dias
case "$mes" in
  "2")
    dies=28;;
  "4"|"6"|"9"|"11")
    dies=30;;
  *)
    dies=31;;
esac
echo "El més: $mes, té $dies dies"
exit 0

