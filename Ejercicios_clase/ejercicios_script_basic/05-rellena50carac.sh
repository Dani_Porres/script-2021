#! /bin/bash
# @Dani_Porres
# 03 Febrero 2022
# Descripcion: rellenar 50 primeros carateres 
#--------------------------------
# xixa

while read -r linea
do
  echo "$linea" | cut -c 1-50
done
exit 0
