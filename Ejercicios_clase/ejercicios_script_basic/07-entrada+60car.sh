#! /bin/bash
# @Dani_Porres
# Febrero 2022
# Programa arguments de mes
#--------------------------------

while read -r line
do
  char=$(echo $line | wc -c)
  if [ $char -gt 60 ]
  then
     echo $line
  fi
done
exit 0

